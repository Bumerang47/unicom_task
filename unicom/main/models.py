from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    company_name = models.CharField('Имя компании', blank=True, null=True, max_length=128)
