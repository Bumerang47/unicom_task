from django.contrib.auth.models import Group
from rest_framework import permissions
from django.http import Http404


def is_in_group(user, group_name):
    """ Takes a user and a group name, and returns `True` if the user is in that group.

    """

    try:
        return Group.objects.get(name=group_name).user_set.filter(id=user.id).exists()
    except Group.DoesNotExist:
        return None


class HasGroupPermission(permissions.BasePermission):
    """ Ensure user is in required groups.

    """
    authenticated_users_only = True

    def has_permission(self, request, view):
        if not request.user or (
           not request.user.is_authenticated and self.authenticated_users_only):
            return False

        # Get a mapping of methods -> required group.
        required_actions_mapping = getattr(view, "required_groups", {})

        # Determine the required groups for this particular request method.
        required_groups = required_actions_mapping.get(view.action, [])

        if (
            any([
                is_in_group(request.user, group_name)
                if group_name != "__all__"
                else True for group_name in required_groups
            ])
            or request.user.is_superuser
        ):
            return True

        if request.method in permissions.SAFE_METHODS:
            # Read permissions already checked and failed, no need
            # to make another lookup.
            raise Http404

        return False
