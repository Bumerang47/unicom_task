from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404

# from django_filters.filters import OrderingFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend, FilterSet, DateTimeFilter

from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action

from main.permissions import HasGroupPermission, is_in_group
from .models import PersonProfile, CreditOffer, RequestOrganization
from .serializers import (
    ProfileSerializer,
    OfferSerializer,
    RequestSerializer,
    RequestUpdateSerializer,
    RequestSendingSerializer
)
from .tasks import receipt_requesting_from_partner, offer_requesting_from_creditor

PERSONAL_FIELDS = (
    'created_date',
    'changed_date',
    'phone_number',
    'last_name',
    'first_name',
    'additional_name',
    'birth_date',
    'phone_number',
    'passport_number',
    'scoring_score',
    'partner',
)

OFFER_FIELDS = (
    'name',
    'created_date',
    'changed_date',
    'start_rotary',
    'finish_rotary',
    'type',
    'request',
)


class PersonProfileFilter(FilterSet):
    changed_date = DateTimeFilter()
    ordering_fields = '__all__',

    class Meta:
        model = PersonProfile
        fields = PERSONAL_FIELDS


class OfferFilter(FilterSet):
    class Meta:
        model = CreditOffer
        fields = OFFER_FIELDS


class PersonProfileViewSet(viewsets.ModelViewSet):
    """ This viewset automatically provides actions for profile from partner.

    """

    queryset = PersonProfile.objects.all()
    serializer_class = ProfileSerializer
    filterset_class = PersonProfileFilter
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    permission_classes = (HasGroupPermission,)
    ordering_fields = PERSONAL_FIELDS
    required_groups = {
        'create': ['partner'],
        'retrieve': ['partner'],
        'list': ['partner'],
        'read': ['partner'],
        'sending_to_creditor': ['partner'],
    }

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = get_user_model().objects.get(id=request.user.pk)
        serializer.validated_data.update({'partner': user})
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def retrieve(self, request, pk=None):
        form = get_object_or_404(self.get_queryset(), pk=pk)
        serializer = self.get_serializer(form)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = super().get_queryset()
        if not self.request.user.is_superuser:
            queryset = queryset.filter(partner__id=self.request.user.pk)
        return queryset

    @action(methods=['patch'], detail=True)
    def sending_to_creditor(self, request, pk=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        receipt_requesting_from_partner.delay(pk, request.user.pk, request.data['creditor'])

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        if self.action == 'sending_to_creditor':
            serializer_class = RequestSendingSerializer
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)


class OfferViewSet(viewsets.ModelViewSet):

    queryset = CreditOffer.objects.all()
    serializer_class = OfferSerializer
    filterset_class = OfferFilter
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    permission_classes = (HasGroupPermission,)
    ordering_fields = OFFER_FIELDS,
    required_groups = {
        'create': ['creditor'],
        'retrieve': ['partner', 'creditor'],
        'list': ['partner', 'creditor'],
    }

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        offer = serializer.save()
        offer_requesting_from_creditor.delay(offer.request.pk)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        queryset = super().get_queryset()
        if not self.request.user.is_superuser:
            if is_in_group(self.request.user, 'creditor'):
                queryset = queryset.filter(request__creditor__id=self.request.user.pk)
            else:
                queryset = queryset.filter(request__person_profile__id=self.request.user.pk)
        return queryset


class RequestViewSet(viewsets.ModelViewSet):
    queryset = RequestOrganization.objects.all()
    serializer_class = RequestSerializer
    partial_serializer_class = RequestUpdateSerializer
    permission_classes = [HasGroupPermission]
    required_groups = {
        'list': ['partner', 'creditor'],
        'retrieve': ['partner', 'creditor'],
        'partial_update': ['creditor'],
        'update': ['creditor'],
        'destroy': ['creditor'],
    }

    def list(self, request):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = super().get_queryset()
        if not self.request.user.is_superuser:
            if is_in_group(self.request.user, 'partner'):
                queryset = queryset.filter(owner__id=self.request.user.pk)
            elif is_in_group(self.request.user, 'creditor'):
                queryset = queryset.filter(creditor__id=self.request.user.pk)
        return queryset

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        if self.action in('update', 'partial_update'):
            serializer_class = self.partial_serializer_class
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)
