from django.contrib import admin
from .models import PersonProfile, CreditOffer, RequestOrganization, OfferTypes, RequestStatus


@admin.register(PersonProfile)
class PersonalApplicationAdmin(admin.ModelAdmin):
    pass


@admin.register(CreditOffer)
class CreditOfferAdmin(admin.ModelAdmin):
    pass


@admin.register(RequestOrganization)
class RequestOrganizationAdmin(admin.ModelAdmin):
    pass


@admin.register(OfferTypes)
class OfferTypesAdmin(admin.ModelAdmin):
    pass


@admin.register(RequestStatus)
class RequestStatusAdmin(admin.ModelAdmin):
    pass
