from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model


def datetime_now():
    return timezone.now().replace(microsecond=0)


class OfferTypes(models.Model):
    key = models.CharField('Ключевое наименование типа', max_length=10, unique=True)
    name = models.CharField('Тип предложения', max_length=16)


class RequestStatus(models.Model):
    DEFAULT_STATUS = {'key': 'new', 'name': 'Новая'}

    key = models.CharField('Ключевое наименование статуса', max_length=10, unique=True)
    name = models.CharField('Статус заявки', max_length=16)
    allowed_statuses = models.ManyToManyField('self', symmetrical=False, blank=True)

    @staticmethod
    def default():
        try:
            status = RequestStatus.objects.get(key=RequestStatus.DEFAULT_STATUS['key'])
        except RequestStatus.DoesNotExist:
            status = RequestStatus(**RequestStatus.DEFAULT_STATUS)
            status.save()
        return status.pk


class PersonProfile(models.Model):
    created_date = models.DateTimeField('Дата создания', default=datetime_now)
    changed_date = models.DateTimeField('Дата изменения', default=datetime_now)
    last_name = models.CharField('Фамилия', max_length=16)
    first_name = models.CharField('Имя', max_length=16)
    additional_name = models.CharField('Отчество', max_length=16, blank=True, null=True)
    birth_date = models.DateField('Дата рождения')
    phone_number = models.CharField('Номер телефона', max_length=10)
    passport_number = models.CharField('Номер паспорта', max_length=10)
    scoring_score = models.CharField('Скоринговый балл', max_length=10)
    partner = models.ForeignKey(get_user_model(), models.DO_NOTHING)  # FIXED related_name

    @property
    def partner_name(self):
        return '{} {}'.format(self.partner.first_name, self.partner.last_name)

    def __str__(self):
        return '"%s" by %s' % (self.first_name, self.partner)


class CreditOffer(models.Model):
    name = models.CharField('Название предложения', max_length=124)
    created_date = models.DateTimeField('Дата создания', default=datetime_now)
    changed_date = models.DateTimeField('Дата изменения', default=datetime_now)
    start_rotary = models.DateTimeField('Начало ротации')
    finish_rotary = models.DateTimeField('Окончание ротации')
    type = models.ForeignKey(OfferTypes, on_delete=models.DO_NOTHING)
    request = models.ForeignKey('RequestOrganization', on_delete=models.DO_NOTHING)


class RequestOrganization(models.Model):
    INDEX_FOR_ALL_CREDITORS = 0

    created_date = models.DateTimeField('Дата создания', default=datetime_now)
    dispatched_date = models.DateTimeField('Дата отправки', default=datetime_now)
    status = models.ForeignKey(RequestStatus, on_delete=models.DO_NOTHING, default=RequestStatus.default)
    person_profile = models.ForeignKey(PersonProfile, on_delete=models.DO_NOTHING)
    owner = models.ForeignKey(get_user_model(), related_name='related_request_owner', on_delete=models.DO_NOTHING)
    # offer = models.ManyToManyField(CreditOffer)
    creditor = models.ForeignKey(get_user_model(), related_name='related_request_creditor', on_delete=models.DO_NOTHING)
