from django.test import TestCase
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate
from kombu.exceptions import OperationalError

from main.models import User
from .models import PersonProfile, RequestOrganization, RequestStatus
from .views import PersonProfileViewSet, RequestViewSet, OfferViewSet
from .tasks import offer_requesting_from_creditor


class PersonProfileTestClass(TestCase):
    fixtures = ['initial']

    def test_list_profile_partner(self):
        factory = APIRequestFactory()
        right_user = User.objects.get(username='partner')
        false_user = User.objects.get(username='creditor')

        user_applications = PersonProfile.objects.filter(partner__pk=right_user.pk)

        list_view = PersonProfileViewSet.as_view({'get': 'list'})
        wsgi_get = factory.get('/applications/')
        force_authenticate(wsgi_get, user=right_user)

        response = list_view(wsgi_get)
        self.assertEqual(response.status_code, 200)

        # receiving a list of only their profiles
        resp_app_id_list = [x.get('id') for x in response.data]
        user_app_id_list = [x.pk for x in user_applications]
        self.assertFalse(list(set(resp_app_id_list) ^ set(user_app_id_list)))

        # receiving a list by a group without access
        force_authenticate(wsgi_get, user=false_user)
        response = list_view(wsgi_get)
        self.assertEqual(response.status_code, 404)

    def test_getting_profile_partner(self):
        factory = APIRequestFactory()
        right_user = User.objects.get(username='partner')

        item_view = PersonProfileViewSet.as_view({'get': 'retrieve'})

        wsgi_get = factory.get('/applications/1/')
        force_authenticate(wsgi_get, user=right_user)
        response = item_view(wsgi_get, pk='1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], 1)

        # Request for a questionnaire created by another user
        wsgi_get = factory.get('/applications/2/')
        force_authenticate(wsgi_get, user=right_user)
        response = item_view(wsgi_get, pk='2')
        self.assertEqual(response.status_code, 404)

    def test_created_profile_partner(self):
        factory = APIRequestFactory()
        right_user = User.objects.get(username='partner')
        false_user = User.objects.get(username='creditor')

        params_application = {
            'last_name': 'Тест',
            'first_name': 'Мен',
            'additional_name': '',
            'birth_date': '1990-01-01',
            'phone_number': '9180001166',
            'passport_number': '4815162342',
            # 'scoring_score': '42',
        }

        # creation with missing parameter
        wsgi_post = factory.post('/applications/', params_application)
        view = PersonProfileViewSet.as_view({'post': 'create'})
        force_authenticate(wsgi_post, user=right_user)
        response = view(wsgi_post)
        self.assertEqual(response.status_code, 400)

        # creation with full parameter
        params_application['scoring_score'] = '42'
        wsgi_post = factory.post('/applications/', params_application)
        force_authenticate(wsgi_post, user=right_user)
        response = view(wsgi_post)
        self.assertEqual(response.status_code, 201)

        # creation without access
        force_authenticate(wsgi_post, user=false_user)
        response = view(wsgi_post)
        self.assertEqual(response.status_code, 403)


class RequestsTestClass(TestCase):
    fixtures = ['initial', 'testing']

    def test_created_requests(self):
        factory = APIRequestFactory()
        partner_user = User.objects.get(username='partner')
        creditor_user = User.objects.get(username='creditor')

        profile = PersonProfile.objects.all().first()
        params = {'person_profile_id': profile.pk}

        # creation with missing parameter
        wsgi_post = factory.post('/requests/', params)
        view = RequestViewSet.as_view({'post': 'create'})
        force_authenticate(wsgi_post, user=partner_user)
        response = view(wsgi_post)
        self.assertEqual(response.status_code, 403)

        # creation with full parameter
        params['creditor'] = creditor_user.pk
        wsgi_post = factory.post('/requests/', params)
        force_authenticate(wsgi_post, user=partner_user)
        response = view(wsgi_post)
        self.assertEqual(response.status_code, 403)

        # creation without access
        force_authenticate(wsgi_post, user=creditor_user)
        response = view(wsgi_post)
        self.assertEqual(response.status_code, 403)

    def test_updated_requests(self):
        factory = APIRequestFactory()
        params = {'status': 'approved'}
        view = RequestViewSet.as_view({'put': 'update', 'patch': 'partial_update'})

        owner_user = User.objects.get(username='partner')
        owner_request = RequestOrganization.objects.filter(owner=owner_user.pk).first()
        wsgi_put = factory.put('/requests/{}/'.format(owner_request.pk), params)
        force_authenticate(wsgi_put, user=owner_user)
        response = view(wsgi_put, pk=owner_request.pk)
        self.assertEqual(response.status_code, 403)

        creditor_user = User.objects.get(username='creditor')
        creditor_request = RequestOrganization.objects.filter(creditor=creditor_user.pk).first()
        wsgi_patch = factory.patch('/requests/{}/'.format(creditor_request.pk), params)
        force_authenticate(wsgi_patch, user=creditor_user)
        response = view(wsgi_patch, pk=creditor_request.pk)
        self.assertEqual(response.status_code, 400)

        params['status'] = 'received'
        wsgi_patch = factory.patch('/requests/{}/'.format(creditor_request.pk), params)
        force_authenticate(wsgi_patch, user=creditor_user)
        response = view(wsgi_patch, pk=creditor_request.pk)
        self.assertEqual(response.status_code, 200)

        super_user = User.objects.get(username='admin')
        wsgi_put = factory.put('/requests/{}/'.format(creditor_request.pk), params)
        force_authenticate(wsgi_put, user=super_user)
        response = view(wsgi_put, pk=creditor_request.pk)
        self.assertEqual(response.status_code, 400)


class OfferTestClass(TestCase):
    fixtures = ['initial', 'testing']

    def test_created_requests(self):
        factory = APIRequestFactory()
        partner_user = User.objects.get(username='partner')
        creditor_user = User.objects.get(username='creditor')
        request = RequestOrganization.objects.filter(creditor_id=creditor_user.pk).first()
        params = {
            'name': 'ПерсональноеПредложение',
            'start_rotary': '2018-10-1 10:00',
            'finish_rotary': '2018-11-1 10:00',
            'request': request.pk
        }

        # creation with missing parameter
        view = OfferViewSet.as_view({'post': 'create'})
        wsgi_post = factory.post('/offers/', params)
        force_authenticate(wsgi_post, user=creditor_user)
        response = view(wsgi_post)
        self.assertEqual(response.status_code, 400)

        # creation with full parameter
        params['type'] = 'credit'
        wsgi_post = factory.post('/offers/', params)
        force_authenticate(wsgi_post, user=creditor_user)

        with self.assertRaises(OperationalError):
            # we mean exception because test without a radis server
            view(wsgi_post)

        request.status = RequestStatus.objects.get(key='received')
        request.save()
        self.assertTrue(offer_requesting_from_creditor(request.pk))
        request.refresh_from_db()
        self.assertEqual(request.status.key, 'approved')
