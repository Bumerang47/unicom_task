from __future__ import absolute_import, unicode_literals
from celery.decorators import task, periodic_task

from main.models import User
from .models import RequestOrganization, RequestStatus, PersonProfile


@task(name="receipt_requesting_from_partner")
def receipt_requesting_from_partner(pk, owner_id, pk_list):
    posted_status = RequestStatus.objects.get(key='posted')
    profile = PersonProfile.objects.get(id=pk)

    if pk_list[0] == RequestOrganization.INDEX_FOR_ALL_CREDITORS:
        creditors = User.objects.filter(group='creditor', is_active=True)
    else:
        creditors = User.objects.filter(pk__in=pk_list, groups__name='creditor', is_active=True)

    for creditor in creditors:
        new_request = RequestOrganization(
            person_profile=profile,
            owner_id=owner_id,
            creditor=creditor,
            status=posted_status
        )
        new_request.save()
        # there may be sending a letter to the lender or another deferred task

    return True


@task(name="offer_requesting_from_creditor")
def offer_requesting_from_creditor(pk):
    request_sheet = RequestOrganization.objects.get(pk=pk)
    request_status = RequestStatus.objects.get(key=request_sheet.status.key)
    next_status = RequestStatus.objects.get(key='approved')

    if not (
        next_status in request_status.allowed_statuses.get_queryset()
        and request_sheet.status.key == 'received'
    ):
        return False

    # there may be sending a letter to the partner or another deferred task
    request_sheet.status = next_status
    request_sheet.save()
    return True
