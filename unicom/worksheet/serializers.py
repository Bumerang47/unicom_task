from rest_framework import serializers

from main.models import User
from . import models


class StatusField(serializers.Field):

    def to_representation(self, obj):
        return obj.name

    def to_internal_value(self, data):
        status = models.RequestStatus.objects.filter(key=data).first()
        if not status:
            self.fail('invalid', input=data)
        return status


class OfferTypeField(serializers.Field):

    def to_representation(self, obj):
        return obj.name

    def to_internal_value(self, data):
        return models.OfferTypes.objects.filter(key=data).first()

    def run_validation(self, data):
        value = self.to_internal_value(data)
        if data and not value:
            raise serializers.ValidationError('Wrong input of type field')
        return value


class ProfileSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(label='ID', read_only=True)
    partner_name = serializers.ReadOnlyField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True, format='%Y-%m-%d %H:%M:%S')
    changed_date = serializers.DateTimeField(read_only=True, format='%Y-%m-%d %H:%M:%S')

    class Meta:
        fields = (
            'id',
            'created_date',
            'changed_date',
            'last_name',
            'first_name',
            'additional_name',
            'birth_date',
            'phone_number',
            'passport_number',
            'scoring_score',
            'partner_name',
        )
        model = models.PersonProfile


class OfferSerializer(serializers.ModelSerializer):
    type = OfferTypeField()
    created_date = serializers.DateTimeField(read_only=True, format='%Y-%m-%d %H:%M:%S')
    changed_date = serializers.DateTimeField(read_only=True, format='%Y-%m-%d %H:%M:%S')
    request = serializers.PrimaryKeyRelatedField(read_only=False, queryset=models.RequestOrganization.objects.all())

    class Meta:
        fields = ('name', 'created_date', 'changed_date', 'start_rotary', 'finish_rotary', 'type', 'request')
        model = models.CreditOffer


class RequestSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)

    status = StatusField(read_only=True)
    created_date = serializers.DateTimeField(read_only=True, format='%Y-%m-%d %H:%M:%S')
    dispatched_date = serializers.DateTimeField(read_only=True, format='%Y-%m-%d %H:%M:%S')
    profile_name = serializers.SerializerMethodField()
    creditor = serializers.SlugRelatedField(slug_field='company_name', read_only=True)
    offer = serializers.SlugRelatedField(slug_field='name', many=True, read_only=True, source='creditoffer_set')

    class Meta:
        fields = (
            'pk',
            'profile_name',
            'status',
            'creditor',
            'created_date',
            'dispatched_date',
            'offer',
        )
        model = models.RequestOrganization

    def validate(self, data):
        new_status = data.get('status').pk if data.get('status') else None
        current_request = self.context['view'].kwargs['pk']
        if new_status and not (
            self.Meta.model.objects.get(pk=current_request)
            .status
            .allowed_statuses
            .filter(pk=new_status)
            .exists()
        ):
            raise serializers.ValidationError("Wrong updating of status field.")

        creditor_list = data.get('creditor')
        if (
            creditor_list
            and not (len(creditor_list) == 1 and creditor_list[0] == self.Meta.model.INDEX_FOR_ALL_CREDITORS)
            and not User.objects.filter(pk__in=creditor_list, groups__name='creditor').exists()
        ):
            raise serializers.ValidationError("Incorrect creditors value")

        return data

    def get_profile_name(self, obj):
        return '{} {}'.format(obj.person_profile.first_name, obj.person_profile.last_name)


class RequestUpdateSerializer(RequestSerializer):
    offer = serializers.RelatedField(write_only=True, queryset=models.CreditOffer.objects.all(), required=False)
    status = StatusField(write_only=True)


class RequestSendingSerializer(RequestSerializer):
    creditor = serializers.ListField(write_only=True)

    class Meta:
        fields = ('creditor',)
        model = models.RequestOrganization
