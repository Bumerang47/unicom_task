"""unicom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers, permissions
from rest_framework.authtoken.views import obtain_auth_token
from worksheet.views import PersonProfileViewSet, OfferViewSet, RequestViewSet
from rest_framework.documentation import include_docs_urls


router = routers.DefaultRouter()
router.register(r'profiles', PersonProfileViewSet)
router.register(r'offers', OfferViewSet)
router.register(r'requests', RequestViewSet)


urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api/token/', obtain_auth_token),
    url(r'^admin/', admin.site.urls),
    url(r'^api/docs/', include_docs_urls(title='Unicom API', permission_classes=(permissions.AllowAny,)))
]
