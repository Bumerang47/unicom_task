# Реализация задачи от Unicom (Django)

### Инсталяция:

`python ./unicom/manage.py makemigrations main`  
`python ./unicom/manage.py makemigrations worksheet`  
`python ./unicom/manage.py migrate`  
`python ./unicom/manage.py loaddata initial`  


### Сборка
`sudo docker-compose build`  
`sudo docker-compose up`  




*Api Root = http://localhost/api/*  
*Api Documents = http://localhost/api/docs*  
__Api Documents используется автоматический с настройками по умолчанию__

Авторизации используется через __Token Authentication__ (http://localhost/api/token),  
Для получение токена по умолчанию можно использовать пользоватлей admin / partner / partner2 / creditor / creditor2 (пароль идентичен логину).


Ряд функций учитывают не все сценации, но удовлетворяют требованиям.  
Покрытие тестами не большое, исключительно в рамках демонстрации.  
В панели администратора возможно редактировать любой объект, но удобство форм редактирования было упущенно.



