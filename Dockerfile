FROM python:3.6
ENV PYTHONUNBUFFERED 1

# RUN echo "Europe/Moscow" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata
WORKDIR /srv/unicom
ADD . /srv/unicom/
COPY requirements.txt /srv/unicom/
RUN pip install -r requirements.txt
